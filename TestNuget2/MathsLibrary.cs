﻿namespace TestNuget2
{
    public static class MathsLibrary
    {
        public static int Sum(int number1, int number2)
        {
            return number1 + number2;
        }

        public static int Subtract(int number1, int number2)
        {
            return number1 - number2;
        }

        public static int Add(int number1, int number2)
        {
            return number1 + number2;
        }

    }
}
